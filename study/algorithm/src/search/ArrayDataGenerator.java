package search;

public class ArrayDataGenerator {
    // 这里定义一个类专门用于生成数组的数据
    // 将构造函数设置为私有的，不能new实例对象
    private ArrayDataGenerator(){}
    public static Integer [] genericArray(int n){
        Integer [] data = new Integer[n];
        for (int i = 0 ; i < n ; i++){
            data[i] = i + 1;
        }
        return data;
    }
}
