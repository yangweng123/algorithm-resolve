package search;

public class LinearSearch {
    // 这里创建一个简单的线性排序算法，定义泛型，可适用于任何数据类型的查找
    public static <E> int search(E[] data, E target) {
        for (int i = 0; i < data.length; i++) {
            if (target.equals(data[i])) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
//        Student [] data = {new Student("杨文光",21),new Student("郝春艳",20)};
//        Object res = LinearSearch.search(data,new Student("郝春艳",20));
//        System.out.println(res);

        // 测试算法的性能（执行时间）
        int[] size = {1000000, 10000000};
        int runs = 100;

        for (int num : size) {
            // 这个是系统类的方法，计算当前的时间戳，以纳秒的形式放回
            Integer[] data1 = ArrayDataGenerator.genericArray(num);
            long start = System.nanoTime();
            for (int i = 0; i < runs; i++) {
                int res = LinearSearch.search(data1, num);
            }
            long end = System.nanoTime();
            double totalTime = (end - start) / 1000000000.0;
            System.out.println("n = " + data1.length + ",runs = " + runs + ",totalTime = " + totalTime);
        }
    }
}
