package search;

import java.util.Objects;

public class Student {
    private String name;
    private int age;
    public void sayHello(){
        System.out.println("大家好，我叫" + this.name + ",今年" + this.age + "岁");
    }

    public Student() {
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age && Objects.equals(name.toLowerCase(), student.name.toLowerCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
