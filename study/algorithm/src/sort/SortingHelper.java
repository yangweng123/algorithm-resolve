package sort;

// 创建这个类用于操作SelectionSort类，实现解耦
public class SortingHelper {
    private SortingHelper() {
    }

    // 实现一个判断数组是否排序的方法
    public static <E extends Comparable<E>> boolean isSort(E[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i].compareTo(arr[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }

    // 实现一个测试算法的时间复杂度的方法
    public static <E extends Comparable<E>> void sortTest(String sortName, E[] arr) {
        long start = System.nanoTime();
        if ("SelectionSort".equals(sortName)) {
            SelectionSort.sort(arr);
        }else if ("InsertSort".equals(sortName)){
            InsertSort.sort(arr);
        }
        long end = System.nanoTime();
        double totalTime = (end - start) / 1000000000.0;
        System.out.println(String.format("%s 算法，排序数组长度 ： %d ，执行时间：%f s", sortName, arr.length, totalTime));
        System.out.print("排序后：");
        showArr(arr);
    }

    // 在这里写一个额外的方法，输出数组里面的值
    public static <E> void showArr(E[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) {
                System.out.print("[ " + arr[i]);
            } else if (i == arr.length - 1) {
                System.out.print(", " + arr[i] + "]");
            } else {
                System.out.print(", " + arr[i]);
            }
        }
        System.out.println();
    }
}
