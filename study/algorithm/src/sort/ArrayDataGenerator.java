package sort;

import java.util.Random;

public class ArrayDataGenerator {
    private ArrayDataGenerator(){}
    public static Integer [] generatorRandomData(int n, int bound){
        Integer [] arr = new Integer[n];
        for (int i = 0; i < n; i++){
            Random r = new Random();
            arr[i] = r.nextInt(bound);
        }
        return arr;
    }
}
