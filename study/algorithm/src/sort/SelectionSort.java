package sort;

public class SelectionSort {
    private SelectionSort(){};
    public static <E extends Comparable> void sort(E [] arr){
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i; j < arr.length; j++) {
                if (arr[minIndex].compareTo(arr[j]) > 0){
                    minIndex = j;
                }
            }
            swap(arr,i,minIndex);
        }
    }

    private static <E> void swap(E[] arr, int i, int minIndex) {
        E temp = arr[i];
        arr[i] = arr[minIndex];
        arr[minIndex] = temp;
    }

}


