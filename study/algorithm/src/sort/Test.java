package sort;

public class Test {
    public static void main(String[] args) {
        // 生产测试数据的部分
        Integer [] arr = ArrayDataGenerator.generatorRandomData(20, 20);
        Student [] arr1 = {
                new Student("杨文光",80),
                new Student("郝春艳",90),
                new Student("憨狗子",0),
                new Student("傻猫子",60)
        };

        // 测试部分
//        SortingHelper.sortTest("SelectionSort",arr);
//        SortingHelper.sortTest("SelectionSort",arr1);
        // 首先生成一个随机数组
        SortingHelper.sortTest("InsertSort",arr);
        SortingHelper.sortTest("InsertSort",arr1);
    }

}
