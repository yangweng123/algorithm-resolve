package array;

public class  Array<E> {
    private E[] data;
    private int size;

    // 无参构造函数，默认容量为10
    public Array() {
        data = (E[]) new Object[10];
    }

    // 有参构造函数，传入数组的容量
    public Array(int capacity) {
        data = (E[]) new Object[capacity];
        size = 0;
    }

    // 获取数组中的元素个数
    public int getSize() {
        return size;
    }

    // 获取数组的容量
    public int getCapacity() {
        return data.length;
    }

    // 判断数组是否为空
    public boolean isEmpty() {
        return size == 0;
    }

    // 向数组的开头添加一个元素
    public void addFirst(E e) {
        add(0, e);
    }

    // 向数组的末尾添加一个元素
    public void addLast(E e) {
        add(size, e);
    }

    // 在数组的指定索引处添加一个元素
    public void add(int index, E e) {
        // 首先需要判断是否超出容量了，如果超过了，那么就进行扩容，实现动态数组
        if (size == data.length) {
            resize(data.length * 2);
        }
        // 然后判断你输入的index是不是合法的
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("index out of bound");
        }
        // 将插入的位置的后面的元素全体向后移一位，然后将e放入index位置
        for (int i = size; i > index; i--) {
            data[i] = data[i - 1];
        }
        data[index] = e;
        // 记得要将size的长度维护一下
        size++;
    }

    // 重写toString方法
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("数组容量：%d ，元素个数：%d\n", data.length, size));
        sb.append("[");
        for (int i = 0; i < size; i++) {
            sb.append(data[i]);
            if (i != size - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    // 通过索引获取指定位置的元素
    public E get(int index){
        if(index < 0 || index >= size){
            throw new IllegalArgumentException("Get Failed.Illegal index");
        }
        return data[index];
    }
    // 通过索引设置指定位置的元素
    public void set(int index, E e){
        if(index < 0 || index >= size){
            throw new IllegalArgumentException("Get Failed.Illegal index");
        }
        data[index] = e;
    }
    // 数组是否包含元素
    public boolean contain(E e){
        for (int i = 0; i < size; i++){
            if (data[i].equals(e)){
                return true;
            }
        }
        return false;
    }
    // 搜索数组中的某个元素：找到了就返回该元素的索引，没找到就返回-1
    public int find(E e){
        for (int i = 0; i < size; i++){
            if (data[i].equals(e)){
                return i;
            }
        }
        return -1;
    }
    // 删除数组中的某个元素，返回删除的元素：只需要将该元素后面的所有元素向前挪一位,如何将size指向前一个元素即可
    public E remove(int index){
        if (index < 0 || index >= size){
            throw new IllegalArgumentException("Illegal index");
        }
        E rem = data[index];
        for (int i = index; i < size - 1; i++){
            data[i] = data[i+1];
        }
        size--;
        // 如果删除之后数组的长度只有容量的一半，就缩容
        if (size == data.length/2){
            resize(size);
        }
        return rem;
    }
    // 删除数组中的第一个元素，返回删除的元素
    public E removeFirst(){
        return remove(0);
    }
    // 删除数组中的最后一个元素，返回删除的元素
    public E removeLast(){
        return remove(size-1);
    }
    // 编写resize扩容方法：创建一个新的数组，长度为newCapacity，如何将原来的元素拷贝到里面去，最后将data指向新的数组
    public void resize(int newCapacity){
        E[] newData = (E[]) new Object[newCapacity];
        for (int i = 0; i < size; i++){
            newData[i] = data[i];
        }
        data = newData;
    }

    // 获取第一个元素
    public E getFirst(){
        return get(0);
    }
    // 获取最后一个元素
    public E getLast(){
        return get(size-1);
    }
}
