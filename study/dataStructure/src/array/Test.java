package array;

public class Test {
    public static void main(String[] args) {
        // 使用了泛型
        Array array = new Array(10);
        for (int i = 0; i < 10; i++){
            array.addLast(i+"杨");
        }
        System.out.println(array);
        array.add(1,20);
        System.out.println(array);
        array.addFirst(-1);
        array.addLast(10);
        System.out.println(array.get(0));
        array.set(0,-100.0);
        array.remove(1);
        System.out.println(array);
    }
}
