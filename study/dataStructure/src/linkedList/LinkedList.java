package linkedList;

public class LinkedList<E> {
    private class Node {
        public Node next;
        public E e;

        public Node(E e, Node next) {
            this.e = e;
            this.next = next;
        }

        public Node(E e) {
            this(e, null);
        }

        public Node() {
            this(null, null);
        }
        @Override
        public String toString(){
            return e.toString();
        }
    }

    private Node dummyHead;
    private int size;

    // 构造链表
    public LinkedList() {
        dummyHead = new Node(null, null);
        size = 0;
    }

    // 获取链表长度的方法
    public int getSize() {
        return size;
    }

    // 判断链表是否为空
    public boolean isEmpty() {
        return size == 0;
    }

    // 在指定位置插入一个元素
    public void add(int index, E e) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("illegal index");
        }
        Node prev = dummyHead;
        for (int i = 0; i < index; i++) {
            prev = prev.next;
        }
        prev.next = new Node(e, prev.next);
        size++;
    }

    // 在链表的头部添加一个元素
    public void addFirst(E e) {
        add(0,e);
    }
    // 在链表的末尾添加一个元素
    public void addLast(E e){
        add(size,e);
    }
    // 获取链表指定位置的元素值
    public E get(int index){
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("illegal index");
        }
        Node current = dummyHead.next;
        for (int i = 0; i < index; i++){
            current = current.next;
        }
        return current.e;
    }
    // 获取链表的第一个元素
    public E getFirst(){
        return get(0);
    }
    // 获取链表的最后一个元素
    public E getLast(){
        return get(size - 1);
    }
    // 更新指定位置的元素值
    public void set(int index, E e){
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("illegal index");
        }
        Node current = dummyHead.next;
        for (int i = 0; i < index; i++){
            current = current.next;
        }
        current.e = e;
    }
    // 判断链表中是否存在元素e
    public boolean contains(E e){
        Node current = dummyHead.next;
        while (current.e != null){
            if (current.e.equals(e)){
                return true;
            }
            current = current.next;
        }
        return false;
    }
    // 删除一个元素，返回这个元素的值
    public E remove(int index){
        if (index < 0 || index > size){
            throw new IllegalArgumentException("illegal index");
        }
        Node prev = dummyHead;
        for (int i = 0; i < index; i++){
            prev = prev.next;
        }
        Node delNode = prev.next;
        prev.next = delNode.next;
        delNode.next = null;
        size--;
        return delNode.e;
    }
    // 删除链表的第一个元素
    public E removeFirst(){
        return remove(0);
    }
    // 删除链表的最后一个元素
    public E removeLast(){
        return remove(size - 1);
    }
    // 重写toString方法
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (Node current = dummyHead.next; current != null; current = current.next){
            sb.append(current + " --> ");
        }
        sb.append("null");
        return sb.toString();
    }
}
