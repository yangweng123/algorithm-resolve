package queue;

import array.Array;

public class ArrayQueue<E> implements Queue<E>{
    private Array<E> array;

    public ArrayQueue() {
        array = new Array<>();
    }

    public ArrayQueue(int capacity) {
        array = new Array<>(capacity);
    }

    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    @Override
    public void enqueue(E e) {
        array.addLast(e);
    }

    // 在这个数组队列中，出栈是从队首出的，所以时间复杂度为O(n)，那么我们可不可以不使用数组里面的移除首元素的方法，
    // 直接用一个变量front指向首部，出栈后，front加一即可，然后尾部用tail变量，如果到了capacity，就使用取余，继续从
    // 头开始利用之前出栈的位置，结束条件就是tail + 1 == front;
    @Override
    public E dequeue() {
        return array.removeFirst();
    }

    @Override
    public E getFront() {
        return array.getFirst();
    }

    // 重写toString方法
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("front [" + array.get(0));
        for (int i = 1; i < getSize(); i++){
            sb.append(", " + array.get(i));
        }
        sb.append("] tail\ncapacity:" + array.getCapacity());
        return sb.toString();
    }
}
