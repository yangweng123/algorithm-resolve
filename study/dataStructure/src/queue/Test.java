package queue;

public class Test {
    public static void main(String[] args) {
//        System.out.println("arrayQueue:");
//        ArrayQueue<Integer> arrayQueue = new ArrayQueue<>();
//        for (int i = 0; i < 10; i++) {
//            arrayQueue.enqueue(i);
//        }
//        System.out.println(arrayQueue);
//        arrayQueue.enqueue(10);
//        System.out.println(arrayQueue);
//        arrayQueue.dequeue();
//        System.out.println(arrayQueue);
//        arrayQueue.enqueue(11);
//        System.out.println(arrayQueue);
//        System.out.println("======================================================");
//        System.out.println("loopQueue:");
//        LoopQueue<Integer> loopQueue = new LoopQueue<>();
//        for (int i = 0; i < 9; i++) {
//            loopQueue.enqueue(i);
//        }
//        System.out.println(loopQueue);
//        loopQueue.enqueue(10);
//        System.out.println(loopQueue);
//        loopQueue.dequeue();
//        loopQueue.dequeue();
//        loopQueue.dequeue();
//        loopQueue.dequeue();
//        System.out.println(loopQueue);
//        loopQueue.enqueue(10);
//        System.out.println(loopQueue);
        LinkedListQueue<Integer> listQueue = new LinkedListQueue<>();
        for (int i = 0; i < 5; i++){
            listQueue.enqueue(i);
            System.out.println(listQueue);
        }
        listQueue.dequeue();
        System.out.println(listQueue);
        System.out.println("是否为空:" + listQueue.isEmpty());
        System.out.println("队列的长度:" + listQueue.getSize());
    }
}
