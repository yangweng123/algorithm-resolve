package stack;

import array.Array;

public class ArrayStack<E> implements Stack<E>{
    private Array<E> array;

    public ArrayStack() {
        array = new Array<>();
    }
    public ArrayStack(int capacity){
        array = new Array<>(capacity);
    }

    @Override
    public int getSize() {
        return array.getSize();
    }

    @Override
    public boolean isEmpty() {
        return array.isEmpty();
    }

    @Override
    public void push(E e) {
        array.addLast(e);
    }

    @Override
    public E pop() {
        return array.removeLast();
    }

    @Override
    public E peek() {
        return array.getLast();
    }

    // 看一下容器大小
    public int getCapacity(){
        return array.getCapacity();
    }

    // 重写toString方法
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Stack: [ " + array.get(0));
        for (int i = 1; i < getSize(); i++){
            sb.append(", " + array.get(i));
        }
        sb.append("] top\ncapacity:" + getCapacity());
        return sb.toString();
    }
}
