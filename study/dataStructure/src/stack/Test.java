package stack;

public class Test {
    public static void main(String[] args) {
//        System.out.println("检测数组栈：");
//        ArrayStack<Integer> arrayStack = new ArrayStack<Integer>();
//        for (int i = 0; i < 10; i++) {
//            arrayStack.push(i);
//        }
//        System.out.println(arrayStack);
//        arrayStack.push(-1);
//        System.out.println(arrayStack);
//        arrayStack.pop();
//        System.out.println(arrayStack);
        System.out.println("检测链表栈：");
        LinkedListStack<Integer> list = new LinkedListStack<>();
        for (int i = 0; i < 5; i++){
            list.push(i);
            System.out.println(list);
        }
        list.pop();
        System.out.println(list);
        System.out.println("链表栈的长度为：" + list.getSize());
        System.out.println("栈顶元素值为：" + list.peek());
    }
}

